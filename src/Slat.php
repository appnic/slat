<?php

namespace appnic\slat;

use appnic\slat\Contracts\TokenManager;
use Illuminate\Support\Facades\Facade;

class Slat extends Facade
{
    protected static function getFacadeAccessor()
    {
        return TokenManager::class;
    }
}