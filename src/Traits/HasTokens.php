<?php

namespace appnic\slat\Traits;

use appnic\slat\Collections\TokenCollection;
use appnic\slat\Contracts\TokenManager;

trait HasTokens
{
    public function tokens() : TokenCollection {
        return resolve(TokenManager::class)->tokens($this);
    }
}
