<?php

namespace appnic\slat;

use appnic\slat\Contracts\TokenManager;
use Illuminate\Support\Arr;
use InvalidArgumentException;

class SlatConfig
{
    protected $driverName;
    protected $driverManager;
    protected $driverOptions;

    public function driver(string $name, string $manager, array $options) {
        if(!class_exists($manager) || !((new $manager) instanceof TokenManager)) {
            throw new InvalidArgumentException(
                sprintf('Class "%s" does not exist or does not implement %s', $manager, TokenManager::class));
        }

        $this->driverName = $name;
        $this->driverManager = $manager;
        $this->driverOptions = $options;

        return $this;
    }

    public function getDriverName() {
        return $this->driverName;
    }

    public function getDriverManager() {
        return $this->driverManager;
    }

    public function getDriverOption(string $name, $default = null) {
        return Arr::get($this->driverOptions, $name, $default);
    }

    public function getDriverOptions() {
        return $this->driverOptions;
    }
}