<?php

namespace appnic\slat\Drivers\Database;

use appnic\slat\Collections\TokenCollection;
use appnic\slat\Contracts\Token;
use appnic\slat\Contracts\TokenManager;
use appnic\slat\Contracts\TokenOwner;
use appnic\slat\Exceptions\TokenOperationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class DatabaseTokenManager implements TokenManager
{
    /**
     * Creates a new Token for the given TokenOwner with an optional token name
     *
     * @param TokenOwner $owner
     * @param string|null $name
     * @return Token
     * @throws TokenOperationException
     */
    public function create(TokenOwner $owner, string $name = null): Token
    {
        $this->checkTokenOwnerType($owner);

        $token = new DatabaseToken(['name' => $name]);
        $token->owner()->associate($owner)->save();

        // Refresh the token before returning to load default values (access_count)
        return $token->refresh();
    }

    /**
     * Marks the token as expired immediately or at the given date.
     *
     * @param Token $token
     * @param Carbon|null $at
     * @return TokenManager
     * @throws TokenOperationException
     */
    public function expire(Token $token, Carbon $at = null): TokenManager
    {
        $this->checkTokenType($token);

        /** @var DatabaseToken $token */
        $token->expired_at = $at ?? now();
        $token->save();

        return $this;
    }

    /**
     * Deletes the given token.
     *
     * @param Token $token
     * @return TokenManager
     * @throws TokenOperationException
     */
    public function delete(Token $token): TokenManager
    {
        $this->checkTokenType($token);

        try {
            /** @var DatabaseToken $token */
            $token->delete();
        } catch (\Exception $e) {
            throw new TokenOperationException("Invalid token model: Needs primary key");
        }

        return $this;
    }

    /**
     * Returns a collection of tokens. Filtered by TokenOwner if one is given
     *
     * @param TokenOwner|null $owner
     * @return TokenCollection
     * @throws TokenOperationException
     */
    public function tokens(TokenOwner $owner = null): TokenCollection
    {
        $this->checkTokenOwnerType($owner);

        if(is_null($owner)) {
            return TokenCollection::fromCollection(DatabaseToken::all());
        }

        /** @var Model $owner */
        return TokenCollection::fromCollection(DatabaseToken::where('owner_id',$owner->getKey())->get());
    }

    /**
     * Returns if a token with the given secret exists
     *
     * @param string $secret
     * @return bool
     */
    public function exists(string $secret): bool
    {
        return !is_null($this->fromSecret($secret));
    }

    /**
     * Returns a token or null for the given secret
     *
     * @param string|null $secret
     * @return Token|null
     */
    public function fromSecret(?string $secret): ?Token
    {
        if(is_null($secret) || empty($secret)) {
            return null;
        }
        return DatabaseToken::where('secret', $secret)->first();
    }

    /**
     * Checks if the given token is an instance of DatabaseToken, otherwise throws an exception
     *
     * @param Token|null $token
     * @throws TokenOperationException
     */
    protected function checkTokenType(?Token $token) {
        if(!is_null($token) && !($token instanceof DatabaseToken)) {
            throw new TokenOperationException('Invalid token object type. Must be instance of '.DatabaseToken::class);
        }
    }

    /**
     * Checks if the given token owner is an instance of Model, otherwise throws an exception
     *
     * @param TokenOwner|null $tokenOwner
     * @throws TokenOperationException
     */
    protected function checkTokenOwnerType(?TokenOwner $tokenOwner) {
        if(!is_null($tokenOwner) && !($tokenOwner instanceof Model)) {
            throw new TokenOperationException('Invalid token owner type. Must be instance of '.Model::class);
        }
    }
}