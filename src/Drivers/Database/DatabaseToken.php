<?php

namespace appnic\slat\Drivers\Database;

use appnic\slat\Contracts\Token;
use appnic\slat\Contracts\TokenOwner;
use appnic\slat\Drivers\Database\Scopes\ExpiredScope;
use appnic\slat\SlatConfig;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class DatabaseToken extends Model implements Token
{
    protected $guarded = ['secret', 'last_accessed_at', 'access_count'];
    protected $hidden = ['id', 'owner_id', 'created_at', 'updated_at'];

    protected $dates = ['last_accessed_at', 'expired_at'];

    /**
     * Booting the token model
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (DatabaseToken $model) {
            $config = resolve(SlatConfig::class);
            $model->secret = Str::random($config->getDriverOption("secret.length", 60));
        });

        static::addGlobalScope(new ExpiredScope);
    }

    public function getTable()
    {
        $config = resolve(SlatConfig::class);
        return $config->getDriverOption('table', 'tokens');
    }

    public function owner() {
        /**
         * @var SlatConfig $config
         */
        $config = resolve(SlatConfig::class);

        return $this->belongsTo(
            $config->getDriverOption('owner_model', \App\User::class),
            'owner_id'
        );
    }

    /**
     * Increases the access counter and sets the "last access" time
     *
     * @return $this
     */
    public function accessed() : Token {
        $this->last_accessed_at = Date::now();
        $this->access_count++;
        $this->save();

        return $this;
    }

    public function getName(): ?String
    {
        return $this->name;
    }

    public function getOwner(): TokenOwner
    {
        return $this->owner;
    }

    public function getLastAccessDate(): ?Carbon
    {
        return $this->last_accessed_at;
    }

    public function hasExpired(): bool
    {
        return !is_null($this->getExpireDate()) && now() >= $this->getExpireDate();
    }

    public function getSecret(): String
    {
        return $this->secret;
    }

    public function getExpireDate(): ?Carbon
    {
        return $this->expired_at;
    }

    public function getAccessCount(): int
    {
        return $this->access_count;
    }
}
