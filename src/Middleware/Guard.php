<?php


namespace appnic\slat\Middleware;

use Illuminate\Http\Request;

class Guard
{
    public function handle(Request $request, \Closure $next, $guard = null) {
        if(in_array($guard, array_keys(config('auth.guards')))) {
            config(['auth.defaults.guard' => $guard]);
        }

        return $next($request);
    }
}