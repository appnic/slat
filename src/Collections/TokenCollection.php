<?php


namespace appnic\slat\Collections;

use appnic\slat\Contracts\Token;
use ArrayObject;
use Illuminate\Support\Collection;

class TokenCollection extends ArrayObject
{
    /**
     * Creates a TokenCollection from multiple tokens.
     *
     * @param Token ...$tokens
     */
    public function __construct(Token ...$tokens)
    {
        parent::__construct($tokens);
    }

    /**
     * Sets the value at the given index
     *
     * @param mixed $index
     * @param mixed $newval
     */
    public function offsetSet($index, $newval)
    {
        if(!($newval instanceof Token)) {
            throw new \InvalidArgumentException("Value must be of type ".Token::class);
        }

        parent::offsetSet($index, $newval);
    }

    /**
     * Appends the elements of an array to this TokenCollection
     *
     * @param array $tokens
     * @return TokenCollection
     */
    public function appendMany(array $tokens) : TokenCollection {
        foreach ($tokens as $token) {
            $this->append($token);
        }

        return $this;
    }

    /**
     * Appends the elements of a Laravel Collection to this TokenCollection
     *
     * @param Collection $collection
     * @return TokenCollection
     */
    public function appendCollection(Collection $collection) : TokenCollection {
        foreach ($collection as $item) {
            $this->append($item);
        }

        return $this;
    }

    /**
     * Creates a TokenCollection from an array
     *
     * @param array $tokens
     * @return TokenCollection
     */
    public static function fromArray(array $tokens) {
        $tokenCollection = new TokenCollection();
        return $tokenCollection->appendMany($tokens);
    }

    /**
     * Creates a TokenCollection from a Laravel Collection
     *
     * @param Collection $collection
     * @return TokenCollection|Collection
     */
    public static function fromCollection(Collection $collection) {
        $tokenCollection = new TokenCollection();
        return $tokenCollection->appendCollection($collection);
    }
}