<?php

namespace appnic\slat\Contracts;

use appnic\slat\Collections\TokenCollection;
use Illuminate\Contracts\Auth\Authenticatable;

interface TokenOwner extends Authenticatable
{
    /**
     * Returns a list of tokens for this TokenOwner
     *
     * @return TokenCollection
     */
    public function tokens() : TokenCollection;
}
