<?php

use appnic\slat\Drivers\Database\DatabaseTokenManager;

return [
    'driver' => 'database',

    'drivers' => [
        'database' => [
            'manager' => DatabaseTokenManager::class,
            'owner_model' => \App\User::class,
            'secret' => [
                'length' => 60
            ],
            'table' => 'tokens'
        ]
    ]
];
