<?php

namespace appnic\slat\Tests\Unit;

use appnic\slat\Contracts\Token;
use appnic\slat\Contracts\TokenManager;
use appnic\slat\Drivers\Database\DatabaseTokenManager;
use appnic\slat\SlatGuard;
use appnic\slat\Tests\Support\Owner;
use appnic\slat\Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SlatGuardTest extends TestCase
{
    /** @var TokenManager */
    public $manager;

    public function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $this->manager = new DatabaseTokenManager();
    }

    public function createTokensForOwnersAndReturnOne(TokenManager $tokenManager, int $numberOfOwners, int $numberOfTokens) : Token {
        $tokens = [];
        for($i = 0; $i<$numberOfOwners; $i++) {
            $name = Str::random(30);
            $owner = Owner::create(['name'=>$name, 'password'=>Hash::make($name.'password')]);

            for($j = 0; $j<$numberOfTokens; $j++) {
                $tokens[] = $tokenManager->create($owner);
            }
        }

        return Arr::random($tokens);
    }

    public function createGuardFromRequest(Request $request, TokenManager $tokenManager) {
        return new SlatGuard($request, app('auth')->createUserProvider('users'), $tokenManager);
    }

    public function testBinding() {
        $this->assertInstanceOf(SlatGuard::class, Auth::guard());
    }

    public function testUserLoggedOut() {
        $guard = $this->createGuardFromRequest(new Request(), $this->manager);

        $this->assertNull($guard->user());
    }

    public function testUser() {
        $token = $this->createTokensForOwnersAndReturnOne($this->manager, 10, 10);

        $request = new Request();
        $request->headers->add(['Authorization'=>'Bearer '.$token->getSecret()]);
        $guard = $this->createGuardFromRequest($request, $this->manager);

        $this->assertSame($token->getOwner()->getAuthIdentifier(), $guard->user()->getAuthIdentifier());
    }

    public function testValidateWithCorrectUserData() {
        $token = $this->createTokensForOwnersAndReturnOne($this->manager, 3, 3);
        $guard = $this->createGuardFromRequest(new Request(), $this->manager);


        /** @var Owner $owner */
        $owner = $token->getOwner();
        $this->assertTrue($guard->validate(['name'=>$owner->name,'password'=>$owner->name.'password']));
    }

    public function testValidateWithWrongUserData() {
        $token = $this->createTokensForOwnersAndReturnOne($this->manager, 3, 3);
        $guard = $this->createGuardFromRequest(new Request(), $this->manager);


        /** @var Owner $owner */
        $owner = $token->getOwner();
        $this->assertFalse($guard->validate(['name'=>$owner->name,'password'=>$owner->name.'wrongpassword']));
    }
}