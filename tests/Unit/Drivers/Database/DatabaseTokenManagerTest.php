<?php

namespace appnic\slat\Tests\Unit;

use appnic\slat\Contracts\Token;
use appnic\slat\Contracts\TokenManager;
use appnic\slat\Drivers\Database\DatabaseTokenManager;
use appnic\slat\Tests\Support\Owner;
use appnic\slat\Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseTokenManagerTest extends TestCase
{
    /** @var Owner */
    public $owner;

    /** @var Token */
    public $token;

    /** @var TokenManager */
    public $tokenManager;

    public function setUp() : void
    {
        parent::setUp();

        $this->tokenManager = new DatabaseTokenManager();
        $this->owner = Owner::create(['name'=>'testowner', 'password'=>Hash::make('testpassword')]);
        $this->token = $this->tokenManager->create($this->owner, 'tokenname');
    }

    public function testCreateToken() {

        $this->assertSame(config('slat.drivers.database.secret.length', 60), strlen($this->token->getSecret()));
        $this->assertEquals($this->token->getOwner()->getAuthIdentifier(), $this->owner->getAuthIdentifier());
        $this->assertSame($this->token->getName(), 'tokenname');
        $this->assertSame(0, $this->token->getAccessCount());
        $this->assertNull($this->token->getLastAccessDate());
        $this->assertFalse($this->token->hasExpired());
        $this->assertNull($this->token->getExpireDate());
    }

    public function testTokensList() {
        /** @var Token $token */
        foreach($this->tokenManager->tokens($this->token->getOwner()) as $token) {
            $this->assertInstanceOf(Token::class, $token);

            if($this->token->getSecret() === $token->getSecret()) {
                return;
            }
        }

        $this->fail('TokenCollection does not contain newly created tokens');
    }

    public function testExpire()
    {
        $this->tokenManager->expire($this->token);
        $this->assertTrue($this->token->hasExpired());
        $this->assertNotNull($this->token->getExpireDate());

        $date = now();
        $this->tokenManager->expire($this->token, $date);
        $this->assertSame($date->toIso8601String(), $this->token->getExpireDate()->toIso8601String());
    }

    public function testFromSecret() {
        $this->assertNotNull($this->tokenManager->fromSecret($this->token->getSecret()));
    }

    public function testExists() {
        $this->assertTrue($this->tokenManager->exists($this->token->getSecret()));
        $this->assertFalse($this->tokenManager->exists(
            Str::random(config('slat.drivers.database.secret.length', 60))));
    }

    public function testDelete() {
        $this->assertTrue($this->tokenManager->exists($this->token->getSecret()));
        $this->tokenManager->delete($this->token);
        $this->assertFalse($this->tokenManager->exists($this->token->getSecret()));
    }
}