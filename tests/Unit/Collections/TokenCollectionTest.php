<?php

namespace appnic\slat\Tests\Unit\Collections;

use appnic\slat\Collections\TokenCollection;
use appnic\slat\Drivers\Database\DatabaseToken;
use appnic\slat\Tests\TestCase;

class TokenCollectionTest extends TestCase
{
    public function testCreateCollectionFromArray()
    {
        $tokenArray = [
            new DatabaseToken(),
            new DatabaseToken(),
            new DatabaseToken()
        ];

        $collection = TokenCollection::fromArray($tokenArray);

        $this->assertSame(count($tokenArray), $collection->count());

        return $collection;
    }

    /**
     * @depends testCreateCollectionFromArray
     */
    public function testAddToCollection(TokenCollection $collection) {
        $originalSize = $collection->count();

        $collection->append(new DatabaseToken());

        $this->assertSame($originalSize+1, $collection->count());
    }

    /**
     * @depends testCreateCollectionFromArray
     */
    public function testAddInvalidObject(TokenCollection $collection) {
        $this->expectException(\InvalidArgumentException::class);
        $collection->append(new \stdClass());
    }
}
