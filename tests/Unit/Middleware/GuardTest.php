<?php

namespace appnic\slat\Tests\Unit\Middleware;

use appnic\slat\Middleware\Guard;
use appnic\slat\Tests\TestCase;

class GuardTest extends TestCase
{
    public $guard;
    public $request;

    protected function setUp() : void
    {
        parent::setUp();

        $this->guard = new Guard();
        $this->request = request();
    }

    public function testHandle()
    {
        $guardName = 'api';

        $this->guard->handle($this->request, function() {}, $guardName);
        $this->assertSame($guardName, config('auth.defaults.guard'));
    }

}