<?php


namespace appnic\slat\Tests;

use appnic\slat\Slat;
use appnic\slat\Tests\Support\Owner;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return ['appnic\slat\Providers\SlatProvider'];
    }

    protected function setUp(): void
    {
        parent::setUp();

        // Load and execute test migrations
        $this->loadMigrationsFrom(__DIR__.'/Support/migrations');
        $this->artisan('migrate', ['--database' => 'testing']);

        // Execute package migrations
        include_once __DIR__."/../database/migrations/create_tokens_table.php.stub";
        (new \CreateTokensTable())->up();
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('auth.providers.users.model', Owner::class);
        $app['config']->set('auth.guards.api.driver', 'slat');
        $app['config']->set('auth.defaults.guard', 'api');

        // Test using a different table name for database driver
        $app['config']->set('slat.drivers.database.table', 'testing_tokens_table');
    }

    protected function getPackageAliases($app)
    {
        return [
            'Slat' => Slat::class
        ];
    }
}